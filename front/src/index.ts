import 'phaser';
import GameConfig = Phaser.Types.Core.GameConfig;
import {DEBUG_MODE, RESOLUTION} from "./Enum/EnvironmentVariable";
import {cypressAsserter} from "./Cypress/CypressAsserter";
import {LoginScene} from "./Phaser/Login/LoginScene";
import {ReconnectingScene} from "./Phaser/Reconnecting/ReconnectingScene";
import {gameManager} from "./Phaser/Game/GameManager";
import {SelectCharacterScene} from "./Phaser/Login/SelectCharacterScene";


const fps : Phaser.Types.Core.FPSConfig = {
    /**
     * The minimum acceptable rendering rate, in frames per second.
     */
    min: 5,
    /**
     * The optimum rendering rate, in frames per second.
     */
    target: 20,
    /**
     * Use setTimeout instead of requestAnimationFrame to run the game loop.
     */
    forceSetTimeOut: true,
    /**
     * Calculate the average frame delta from this many consecutive frame intervals.
     */
    deltaHistory: 120,
    /**
     * The amount of frames the time step counts before we trust the delta values again.
     */
    panicMax: 20,
    /**
     * Apply delta smoothing during the game update to help avoid spikes?
     */
    smoothStep: false
}

const config: GameConfig = {
    type: Phaser.CANVAS,
    title: "Office game",
    width: window.innerWidth / RESOLUTION / 2,
    height: window.innerHeight / RESOLUTION,
    parent: "game-overlay",
    scene: [LoginScene, SelectCharacterScene, ReconnectingScene],
    zoom: RESOLUTION,
    fps: fps,
    //dom: {
     //   createContainer: true
    //},
    render: {
        pixelArt: false,
        roundPixels: true,
        antialias: false
    },
    physics: {
        default: "arcade",
        arcade: {
            debug: DEBUG_MODE
        }
    }
};

cypressAsserter.gameStarted();

let game = new Phaser.Game(config);

window.addEventListener('resize', function (event) {
    game.scale.resize(window.innerWidth / RESOLUTION /2, window.innerHeight / RESOLUTION);
});
