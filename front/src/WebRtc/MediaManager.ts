let videoConstraint: boolean|MediaTrackConstraints = {
    width: { min: 320, ideal: 640, max: 640 },
    height: { min: 240, ideal: 360 },
    frameRate: { min: 10, ideal: 15, max: 30 },
    facingMode: "user",
    resizeMode: 'crop-and-scale',
    aspectRatio: 1.777777778
};
export class MediaManager {
    localStream: MediaStream|null = null;
    remoteVideo: Array<any> = new Array<any>();
    myCamVideo: HTMLVideoElement;
    cinemaClose: any = null;
    cinema: any = null;
    microphoneClose: any = null;
    microphone: any = null;
    webrtcInAudio: HTMLAudioElement;
    constraintsMedia : {audio : any, video : any} = {
        audio: true,
        video: videoConstraint
    };
    updatedLocalStreamCallBack : Function;

    constructor(updatedLocalStreamCallBack : Function) {
        this.updatedLocalStreamCallBack = updatedLocalStreamCallBack;

        this.myCamVideo = this.getElementByIdOrFail<HTMLVideoElement>('myCamVideo');
        this.webrtcInAudio = this.getElementByIdOrFail<HTMLAudioElement>('audio-webrtc-in');
        this.webrtcInAudio.volume = 0.2;

        this.microphoneClose = document.getElementById('microphone-close');
        this.microphoneClose.style.display = "none";
        this.microphoneClose.addEventListener('click', (e: any) => {
            e.preventDefault();
            this.enabledMicrophone();
            //update tracking
        });
        this.microphone = document.getElementById('microphone');
        this.microphone.addEventListener('click', (e: any) => {
            e.preventDefault();
            this.disabledMicrophone();
            //update tracking
        });

        this.cinemaClose = document.getElementById('cinema-close');
        this.cinemaClose.style.display = "none";
        this.cinemaClose.addEventListener('click', (e: any) => {
            e.preventDefault();
            this.enabledCamera();
            //update tracking
        });
        this.cinema = document.getElementById('cinema');
        this.cinema.addEventListener('click', (e: any) => {
            e.preventDefault();
            this.disabledCamera();
            //update tracking
        });
    }

    activeVisio(){
        let webRtc = this.getElementByIdOrFail('webRtc');
        webRtc.classList.add('active');
    }

    enabledCamera() {
        this.cinemaClose.style.display = "none";
        this.cinema.style.display = "block";
        this.constraintsMedia.video = videoConstraint;
        this.getCamera().then((stream) => {
            this.updatedLocalStreamCallBack(stream);
        });
    }

    disabledCamera() {
        this.cinemaClose.style.display = "block";
        this.cinema.style.display = "none";
        this.constraintsMedia.video = false;
        this.myCamVideo.srcObject = null;
        if (this.localStream) {
            this.localStream.getVideoTracks().forEach((MediaStreamTrack: MediaStreamTrack) => {
                MediaStreamTrack.stop();
            });
        }
        this.getCamera().then((stream) => {
            this.updatedLocalStreamCallBack(stream);
        });
    }

    enabledMicrophone() {
        this.microphoneClose.style.display = "none";
        this.microphone.style.display = "block";
        this.constraintsMedia.audio = true;
        this.getCamera().then((stream) => {
            this.updatedLocalStreamCallBack(stream);
        });
    }

    disabledMicrophone() {
        this.microphoneClose.style.display = "block";
        this.microphone.style.display = "none";
        this.constraintsMedia.audio = false;
        if(this.localStream) {
            this.localStream.getAudioTracks().forEach((MediaStreamTrack: MediaStreamTrack) => {
                MediaStreamTrack.stop();
            });
        }
        this.getCamera().then((stream) => {
            this.updatedLocalStreamCallBack(stream);
        });
    }

    //get camera
    getCamera() {
        let promise = null;
        try {
            promise = navigator.mediaDevices.enumerateDevices()
                .then(devices => {
                    let cams = devices.filter(device => device.kind == "videoinput");
                    if (cams.length == 0){
                        this.constraintsMedia.video = false;
                        document.getElementsByClassName("btn-video")[0].style.display = "none";
                        document.getElementById("div-myCamVideo").style.display = "none";
                    }
                    return navigator.mediaDevices.getUserMedia(this.constraintsMedia).then((stream: MediaStream) => {
                        this.localStream = stream;
                        this.myCamVideo.srcObject = this.localStream;
                        return stream;
                    }).catch((err) => {
                        console.info(`error get media {video: ${this.constraintsMedia.video}},{audio: ${this.constraintsMedia.audio}}`,err);
                        this.localStream = null;
                    })
                })

        } catch (e) {
            promise = Promise.reject(false);
        }
        return promise;
    }

    /**
     *
     * @param userId
     */
    addActiveVideo(userId : string, userName: string = ""){
        this.webrtcInAudio.play();
        let elementRemoteVideo = this.getElementByIdOrFail("activeCam");
        userName = userName.toUpperCase();
        let color = this.getColorByString(userName);
        elementRemoteVideo.insertAdjacentHTML('beforeend', `
            <div id="div-${userId}" class="video-container" style="border-color: ${color};">
                <div class="connecting-spinner"></div>
                <div class="rtc-error" style="display: none"></div>
                <i style="background-color: ${color};">${userName}</i>
                <img id="microphone-${userId}" src="resources/logos/microphone-close.svg">
                <video id="${userId}" autoplay></video>
            </div>
        `);
        this.remoteVideo[(userId as any)] = document.getElementById(userId);
    }

    /**
     *
     * @param userId
     */
    disabledMicrophoneByUserId(userId: string){
        let element = document.getElementById(`microphone-${userId}`);
        if(!element){
            return;
        }
        element.classList.add('active')
    }

    /**
     *
     * @param userId
     */
    enabledMicrophoneByUserId(userId: string){
        let element = document.getElementById(`microphone-${userId}`);
        if(!element){
            return;
        }
        element.classList.remove('active')
    }

    /**
     *
     * @param userId
     */
    disabledVideoByUserId(userId: string) {
        let element = document.getElementById(`${userId}`);
        if (element) {
            element.style.opacity = "0";
        }
        element = document.getElementById(`div-${userId}`);
        if (!element) {
            return;
        }
        element.style.borderStyle = "solid";
    }

    /**
     *
     * @param userId
     */
    enabledVideoByUserId(userId: string){
        let element = document.getElementById(`${userId}`);
        if(element){
            element.style.opacity = "1";
        }
        element = document.getElementById(`div-${userId}`);
        if(!element){
            return;
        }
        element.style.borderStyle = "none";
    }

    /**
     *
     * @param userId
     * @param stream
     */
    addStreamRemoteVideo(userId : string, stream : MediaStream){
        this.remoteVideo[(userId as any)].srcObject = stream;
    }

    /**
     *
     * @param userId
     */
    removeActiveVideo(userId : string){
        let element = document.getElementById(`div-${userId}`);
        if(!element){
            return;
        }
        element.remove();
    }

    isConnecting(userId : string): void {
        let connectingSpinnerDiv = this.getSpinner(userId);
        if (connectingSpinnerDiv === null) {
            return;
        }
        connectingSpinnerDiv.style.display = 'block';
    }

    isConnected(userId : string): void {
        let connectingSpinnerDiv = this.getSpinner(userId);
        if (connectingSpinnerDiv === null) {
            return;
        }
        connectingSpinnerDiv.style.display = 'none';
    }

    isError(userId : string): void {
        let element = document.getElementById(`div-${userId}`);
        if(!element){
            return;
        }
        let errorDiv = element.getElementsByClassName('rtc-error').item(0) as HTMLDivElement|null;
        if (errorDiv === null) {
            return;
        }
        errorDiv.style.display = 'block';
    }

    private getSpinner(userId : string): HTMLDivElement|null {
        let element = document.getElementById(`div-${userId}`);
        if(!element){
            return null;
        }
        let connnectingSpinnerDiv = element.getElementsByClassName('connecting-spinner').item(0) as HTMLDivElement|null;
        return connnectingSpinnerDiv;
    }

    /**
     *
     * @param str
     */
    private getColorByString(str: String) : String|null {
        let hash = 0;
        if (str.length === 0) return null;
        for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
            hash = hash & hash;
        }
        let color = '#';
        for (let i = 0; i < 3; i++) {
            let value = (hash >> (i * 8)) & 255;
            color += ('00' + value.toString(16)).substr(-2);
        }
        return color;
    }

    private getElementByIdOrFail<T extends HTMLElement>(id: string): T {
        let elem = document.getElementById(id);
        if (elem === null) {
            throw new Error("Cannot find HTML element with id '"+id+"'");
        }
        // FIXME: does not check the type of the returned type
        return elem as T;
    }

}
